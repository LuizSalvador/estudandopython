from flask import Flask, render_template, request, url_for, redirect, jsonify
from flask_sqlalchemy import SQLAlchemy

app = Flask(__name__)
app.config['SQLALCHEMY_DATABASE_URI'] = 'sqlite:///meubanco2.db'

db = SQLAlchemy(app)

class Cliente(db.Model):
	__tablename__ = 'cliente2'
	_id = db.Column(db.Integer, primary_key=True, autoincrement=True)
	nome = db.Column(db.String)
	email = db.Column(db.String)
	
	def __init__(self, nome, email):
		self.nome = nome
		self.email = email
		
@app.route('/index', methods=['GET'])
def home():
	return 'Luiz'
	
@app.route('/cliente')
def retorna_cliente():
	clientes = Cliente.query.all()
	retorno = []
	for cliente in clientes :
	retorno_dado = {}
	retorno_dado['nome'] = cliente.nome
	retorno.append(retorno_dado)
return jsonify(retorno)

@app.route('/cliente'), methods=['POST'])
def insere_cliente()
	clientes = request.get_json()
	novo_cliente = Cliente(nome=clientes['nome'], email=clientes['email'])
	db.session.add(novo_cliente)
	db.session.commit()
return jsonify({'message' : 'Cliente Cadastrado!'}), 201
	
	
@app.route('/cliente/<int:id_cliente>', methods=['PUT'])
def atualiza(id_cliente):
	dados = request.get_json()
	cliente = Cliente.query.filter_by(id=id_cliente).first
	cliente.nome = dados['nome']
	db.session.commit()
return jsonify({'message': 'Cliente Atualizado!'}), 200
	

if __name__ == "__main__" :
 app.run(debug=True)

	