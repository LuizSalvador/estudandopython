from flask import Flask, render_template, request, url_for, redirect, jsonify
from flask_sqlalchemy import SQLAlchemy

app = Flask(__name__)
app.config['SQLALCHEMY_DATABASE_URI'] = 'sqlite:///meubanco.db'

db = SQLAlchemy(app)

class Pessoa(db.Model):
    __tablename__ = 'cliente'
    _id = db.Column(db.Integer, primary_key=True, autoincrement=True)
    nome = db.Column(db.String)
    telefone = db.Column(db.String)
    email = db.Column(db.String)
    cpf = db.Column(db.String)

    def __init__(self, nome, telefone, email, cpf):
        self.nome = nome
        self.telefone = telefone
        self.email = email
        self.cpf = cpf

@app.route("/index", methods=['GET'])
def index():
	return 'oi'

@app.route('/pessoa')
def get_all_pessoas():
	pessoas = Pessoa.query.all()
	retorno = []
	for pessoa in pessoas:
		retorno_dado = {}
		retorno_dado['nome'] = pessoa.nome
		retorno_dado['telefone'] = pessoa.telefone
		retorno.append(retorno_dado)
		
	return jsonify(retorno)
	
@app.route('/pessoa', methods=['POST'])
def insert_pessoa():
	dados = request.get_json()
	nova_pessoa = Pessoa(nome=dados['nome'], telefone=dados['telefone'], email=dados['email'], cpf=dados['cpf'])
	db.session.add(nova_pessoa)
	db.session.commit()
	return jsonify({'message': 'Pessoa criada'}), 201

@app.route('/pessoa/<int:id_pessoa>')
def get_one_pessoa(id_pessoa):
	pessoa = Pessoa.query.filter_by(_id=id_pessoa).first()
	retorno_dado = {}
	retorno_dado['nome'] = pessoa.nome
	retorno_dado['telefone'] = pessoa.telefone
	return jsonify({'pessoa': retorno_dado}), 200

@app.route('/pessoa/<int:id_pessoa>', methods=['PUT'])
def atualiza_telefone(id_pessoa):
	dados = request.get_json()
	pessoa = Pessoa.query.filter_by(_id=id_pessoa).first()
	pessoa.telefone = dados['telefone']
	db.session.commit()
	return jsonify({'message': 'Telefone atualizado'}), 200
	
if __name__ == "__main__":
    app.run(debug=True)
